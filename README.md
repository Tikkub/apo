# Apo

## Description
* Fast ECS (Entity Component System)
* Entities are simple int32
* Components are pure data
* Systems contain all the logic
* Inspired by http://entity-systems.wikidot.com/fast-entity-component-system#java

## Advantage
* Alternative to GameObject Component provided by Unity
* More control over the update loop
* More control over the event flow
* Easier to multithread
* Easier to manage memory with pooling
* Easier to load/save data

## Drawback
* You have to rethink the way you develop games
* Thinking parallelization instead of hierarchy is not very natural

## Results
* https://www.youtube.com/watch?v=-A_VEnDf1Ng
* https://www.youtube.com/watch?v=fryX28vvHMc