﻿namespace D3
{
    using UnityEngine;
    using UnityEditor;
    using UnityEditorInternal;

    public class TableAssetEditor : Editor
    {
        private const string ARRAY_PROPERTY_NAME = "entries";
        private const int LIST_ELEMENT_LABEL_WIDTH = 20;
        private ReorderableList list;

        private void OnEnable()
        {
            list = new ReorderableList(serializedObject, serializedObject.FindProperty(ARRAY_PROPERTY_NAME), true, true, true, true);
            list.drawHeaderCallback = DrawHeaderCallback;
            list.drawElementCallback = DrawElementCallback;
        }

        private void DrawHeaderCallback(Rect rect)
        {
            //EditorGUI.LabelField(rect, "Materials");
        }

        private void DrawElementCallback(Rect rect, int index, bool active, bool focused)
        {
            EditorGUI.LabelField(
                new Rect(rect.x, rect.y, LIST_ELEMENT_LABEL_WIDTH, EditorGUIUtility.singleLineHeight),
                (index + 1).ToString()
            );
            EditorGUI.ObjectField(
                new Rect(rect.x + LIST_ELEMENT_LABEL_WIDTH, rect.y, rect.width - LIST_ELEMENT_LABEL_WIDTH, EditorGUIUtility.singleLineHeight),
                list.serializedProperty.GetArrayElementAtIndex(index),
                GUIContent.none
            );
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            serializedObject.Update();
            list.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
            EditorGUILayout.Space();
        }
    }
}