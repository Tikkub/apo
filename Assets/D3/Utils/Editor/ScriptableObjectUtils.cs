﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace D3
{
    public static class ScriptableObjectUtils
    {
        public static T CreateAsset<T>() where T : ScriptableObject
        {
            return CreateAsset<T>(null, null);
        }

        public static T CreateAsset<T>(string name, string path) where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            if (name == null)
            {
                string className = typeof(T).ToString();
                // Only take last occurence
                int sepIndex = className.IndexOf("+");
                if (sepIndex > -1)
                {
                    className = className.Substring(sepIndex + 1, className.Length - sepIndex - 1);
                }
                name = "New " + className;
            }

            string assetPathAndName = AssetUtils.GeneratePath(name, path);

            if (asset == null)
            {
                Debug.Log("Could not create instance of '" + typeof(T).ToString() + "'");
                return null;
            }

            // TODO: Try/catch message for invalid folders

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;

            return asset;
        }
    }
}