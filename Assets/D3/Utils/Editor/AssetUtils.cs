﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityObject = UnityEngine.Object;

namespace D3
{
    public class AssetUtils
    {
        public static string GeneratePath(string name, string path = null, string extension = "asset")
        {
            if (path == null)
            {
                path = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (path == "")
                {
                    path = "Assets";
                }
                else if (Path.GetExtension(path) != "")
                {
                    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
                }
            }

            return AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + "." + extension);
        }

        public static T[] GetAllOfType<T>(string pathFilter = null, bool searchPrefab = false) where T : UnityObject
        {
            return Array.ConvertAll(GetAllOfType(typeof(T), pathFilter, searchPrefab), item => (T)item);
        }

        public static UnityObject[] GetAllOfType(Type type, string pathFilter = null, bool searchPrefab = false)
        {
            string[] guids = AssetDatabase.FindAssets("t:" + type.Name);
            List<UnityObject> assets = new List<UnityObject>();

            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                if (pathFilter != null && !path.ToLower().Contains(pathFilter.ToLower())) continue;
                assets.Add(AssetDatabase.LoadAssetAtPath(path, type));
            }

            if (searchPrefab)
            {
                guids = AssetDatabase.FindAssets("t:Prefab");
                for (int i = 0; i < guids.Length; i++)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                    UnityObject[] unityObjects = AssetDatabase.LoadAllAssetsAtPath(path);
                    foreach (UnityObject unityObject in unityObjects)
                    {
                        if (unityObject is GameObject)
                        {
                            GameObject go = (GameObject)unityObject;
                            Component c = go.GetComponent(type);
                            if (c != null)
                            {
                                assets.Add(c);
                            }
                        }
                    }
                }
            }

            return assets.ToArray();
        }

        public static Vector2 GetImageSize(TextureImporter textureImporter)
        {
            object[] args = new object[2] { 0, 0 };
            MethodInfo mi = typeof(TextureImporter).GetMethod("GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance);
            mi.Invoke(textureImporter, args);
            return new Vector2((int)args[0], (int)args[1]);
        }

        /*
        http://forum.unity3d.com/threads/getting-original-size-of-texture-asset-in-pixels.165295/
        public static Vector2 GetTextureSize(Texture2D asset)
        {
            if (asset != null)
            {
                string assetPath = AssetDatabase.GetAssetPath(asset);
                TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;

                if (importer != null)
                {
                    object[] args = new object[2] { 0, 0 };
                    MethodInfo mi = typeof(TextureImporter).GetMethod("GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance);
                    mi.Invoke(importer, args);

                    width = (int)args[0];
                    height = (int)args[1];

                    return true;
                }
            }

            height = width = 0;
            return false;
        }*/
    }

    public static class AssetExtensions
    {
        public static bool IsInPrefab(this Transform t)
        {
            return !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(t.root.gameObject));
        }

        public static GameObject GetPrefab(this Transform t)
        {
            if (!t.IsInPrefab()) return null;
            while (t.parent != null)
            {
                t = t.parent;
            }
            return t.gameObject;
        }

        public static GameObject GetPrefab(this UnityEngine.Object o)
        {
            if (o is GameObject) return ((GameObject)o).transform.GetPrefab();
            if (o is Component) return ((Component)o).transform.GetPrefab();
            return null;
        }
    }
}