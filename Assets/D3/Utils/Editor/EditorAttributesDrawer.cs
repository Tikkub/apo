﻿using UnityEditor;
using UnityEngine;

namespace D3
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }

    [CustomPropertyDrawer(typeof(RangeStepAttribute))]
    internal sealed class RangeStepDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var rangeAttribute = (RangeStepAttribute)base.attribute;

            if (property.propertyType == SerializedPropertyType.Integer)
            {
                int value = EditorGUI.IntSlider(position, label, property.intValue, rangeAttribute.min, rangeAttribute.max);
                value = (value / rangeAttribute.step) * rangeAttribute.step;
                property.intValue = value;
            }
            else
            {
                EditorGUI.LabelField(position, label.text, "Use RangeStepDrawer with float or int.");
            }
        }
    }

    [CustomPropertyDrawer(typeof(RangePowerOfTwoAttribute))]
    internal sealed class RangePowerOfTwoDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var rangeAttribute = (RangePowerOfTwoAttribute)base.attribute;

            if (property.propertyType == SerializedPropertyType.Integer)
            {
                int value = EditorGUI.IntSlider(position, label, property.intValue, rangeAttribute.min, rangeAttribute.max);
                property.intValue = value = Mathf.ClosestPowerOfTwo(value);
            }
            else
            {
                EditorGUI.LabelField(position, label.text, "Use RangePowerOfTwo with integer.");
            }
        }
    }
}