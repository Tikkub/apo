﻿using UnityEngine;

namespace D3
{
    public class Vector3Utils
    {
        public static Vector3 Floor(Vector3 v)
        {
            return new Vector3(Mathf.Floor(v.x), Mathf.Floor(v.y), Mathf.Floor(v.z));
        }

        public static Vector3 Sign(Vector3 v)
        {
            return new Vector3(v.x > 0 ? 1 : (v.x < 0 ? -1 : 0), v.y > 0 ? 1 : (v.y < 0 ? -1 : 0), v.z > 0 ? 1 : (v.z < 0 ? -1 : 0));
        }

        public static Vector3 Abs(Vector3 v)
        {
            return new Vector3(v.x > 0 ? v.x : -v.x, v.y > 0 ? v.y : -v.y, v.z > 0 ? v.z : -v.z);
        }

        public static Vector3 Divide(Vector3 vector1, Vector3 byVector2)
        {
            return new Vector3(vector1.x / byVector2.x, vector1.y / byVector2.y, vector1.z / byVector2.z);
        }

        public static Vector3 Multiply(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
        }

        public static Vector3 AssignIfSign(Vector3 v, Vector3 ifPositive, Vector3 ifNegative)
        {
            return new Vector3(v.x > 0 ? ifPositive.x : ifNegative.x, v.y > 0 ? ifPositive.y : ifNegative.y, v.z > 0 ? ifPositive.z : ifNegative.z);
        }
    }


    public static class Vector3Extensions
    {
        public static Vector3 Floor(this Vector3 v)
        {
            v.x = Mathf.Floor(v.x);
            v.y = Mathf.Floor(v.y);
            v.z = Mathf.Floor(v.z);
            return v;
        }

        public static Vector3 Sign(this Vector3 v)
        {
            v.x = v.x > 0 ? 1 : (v.x < 0 ? -1 : 0);
            v.y = v.y > 0 ? 1 : (v.y < 0 ? -1 : 0);
            v.z = v.z > 0 ? 1 : (v.z < 0 ? -1 : 0);
            return v;
        }

        public static Vector3 Abs(this Vector3 v)
        {
            v.x = v.x > 0 ? v.x : -v.x;
            v.y = v.y > 0 ? v.y : -v.y;
            v.z = v.z > 0 ? v.z : -v.z;
            return v;
        }

        public static Vector3 Divide(this Vector3 v, Vector3 by)
        {
            v.x = v.x / by.x;
            v.y = v.y / by.y;
            v.z = v.z / by.z;
            return v;
        }

        public static Vector3 Multiply(this Vector3 v, Vector3 by)
        {
            v.x = v.x * by.x;
            v.y = v.y * by.y;
            v.z = v.z * by.z;
            return v;
        }
    }
}