﻿using UnityEngine;

namespace D3
{
    public class StringUtils : MonoBehaviour
    {
    }

    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
    }
}