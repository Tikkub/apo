﻿namespace D3
{
    public class BitSection
    {
        public int position;
        public int length;
        public int mask;
        public int maskInverse;

        public BitSection(int length)
        {
            this.position = 0;
            this.length = length;
            this.mask = ((1 << length) - 1) << position;
            this.maskInverse = ~this.mask;
        }

        public BitSection(int length, BitSection previousSection)
        {
            this.position = previousSection.position + previousSection.length;
            this.length = length;
            this.mask = ((1 << length) - 1) << position;
            this.maskInverse = ~this.mask;
        }

        public int GetMaxValue()
        {
            return GetValueCount() - 1;
        }

        public int GetValueCount()
        {
            return 1 << length;
        }

        public int GetRandomValue()
        {
            return RandomUtils.RandomInt(1 << length);
        }
    }
}