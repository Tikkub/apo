﻿using UnityEngine;

namespace D3
{
    public class GameObjectUtils : MonoBehaviour
    {
    }

    public static class GameObjectExtensions
    {
        public static void DestroyAllChilden(this Component c)
        {
            foreach (Transform child in c.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        public static void DestroyAllChilden(this GameObject go)
        {
            DestroyAllChilden(go.transform);
        }
    }
}