﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace D3
{
    public static class TypeUtils
    {
        public static List<Type> allTypes = new List<Type>();
        public static bool initialized = false;

        private static Dictionary<string, Type> typesByFullName = new Dictionary<string, Type>();
        private static Type type;

        static TypeUtils()
        {
            if (initialized) return;

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (!typesByFullName.ContainsKey(type.FullName))
                    {
                        allTypes.Add(type);
                        typesByFullName.Add(type.FullName, type);
                    }
                }
            }
            initialized = true;
        }

        public static List<Type> GetTypesByName<T>(string name = null)
        {
            Type parentType = typeof(T);
            if (name != null)
            {
                name = name.ToLower();
            }

            List<Type> result = new List<Type>();
            for (int i = 0; i < allTypes.Count; i++)
            {
                type = allTypes[i];
                if (!parentType.IsAssignableFrom(type) || type.Equals(parentType)) continue;

                if (name == null || type.Name.ToLower() == name || type.FullName.ToLower() == name)
                {
                    result.Add(type);
                }
            }

            return result;
        }

        public static Type GetTypeByName(string name)
        {
            if (!typesByFullName.ContainsKey(name)) return null;
            return typesByFullName[name];
        }

        public static List<Type> FindTypesContaining<T>(string search)
        {
            Type parentType = typeof(T);
            search = search.ToLower();

            List<Type> result = new List<Type>();
            for (int i = 0; i < allTypes.Count; i++)
            {
                type = allTypes[i];

                if (!parentType.IsAssignableFrom(type)) continue;

                if (type.FullName.ToLower().Equals(search))
                {
                    result.Clear();
                    result.Add(type);
                    return result;
                }

                if (type.FullName.ToLower().Contains(search))
                {
                    result.Add(type);
                }
            }

            return result;
        }

        public static List<Type> FindTypeAssignableFor(Type assignable)
        {
            List<Type> result = new List<Type>();

            foreach (Type type in allTypes)
            {
                if (type == assignable) continue;
                if (type.IsAbstract) continue;
                if (assignable.IsAssignableFrom(type))
                {
                    result.Add(type);
                }
            }

            return result;
        }
    }
}