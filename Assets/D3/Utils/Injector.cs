﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace D3
{
    public static class Injector
    {
        private static Dictionary<Type, object> bindingsByType = new Dictionary<Type, object>();
        private static Dictionary<string, object> bindingsByName = new Dictionary<string, object>();

        public static ReflectedType<Inject> reflection = new ReflectedType<Inject>();

        public static void Inject(this object o)
        {
            // Get the reflection data for the type of the object
            ReflectedType<Inject> reflectedType = reflection.Get(o.GetType());

            // Browse the injectable field of the type
            for (int i = 0; i < reflectedType.fields.Length; i++)
            {
                // Get the current field infos
                ReflectedField<Inject> reflectedField = reflectedType.fields[i];

                // Create a holder for the value to inject
                object value = null;

                // If the attribute has an identifier
                if (string.IsNullOrEmpty(reflectedField.attribute.identifier))
                {
                    // Get the value by type
                    bindingsByType.TryGetValue(reflectedField.type, out value);
                }
                // Else 
                else
                {
                    // Get the value by name
                    bindingsByName.TryGetValue(reflectedField.attribute.identifier, out value);
                }

                // If the value is null and the field does not allowed null
                if (value == null && !reflectedField.attribute.allowNull)
                {
                    Debug.LogError(String.Format("Injector : Trying to inject a null value in a {0} field type. Check your binding order or set allowNull to true in the Inject attribute.", o.GetType()));
                }

                // Set the value to the object
                reflectedField.fieldInfo.SetValue(o, value);
            }
        }

        public static void Bind(object o)
        {
            // Bind the value with the type
            BindInternal(o.GetType(), o);
        }

        public static void Bind<T>(object o)
        {
            // Bind the value with the type
            BindInternal(typeof(T), o);
        }

        public static void Unbind(object o)
        {
            // Unbind the type
            UnbindInternal(o.GetType());
        }

        public static void Unbind<T>()
        {
            // Unbind the type
            UnbindInternal(typeof(T));
        }

        private static void BindInternal(Type type, object value)
        {
            // Safety to avoid multiple binding of the same type
            if (bindingsByType.ContainsKey(type))
            {
                // Show a message
                Debug.LogWarning(String.Format("Injector : Trying to rebind object of type {0} Please fix it!", type));

                // Remove the previous binding
                bindingsByType.Remove(type);
            }

            // Set the new binding
            bindingsByType.Add(type, value);
        }

        private static void UnbindInternal(Type type)
        {
            // Safety when removing unexistant binding
            if (!bindingsByType.ContainsKey(type))
            {
                // Show a message
                Debug.LogWarning(String.Format("Injector : Trying to unbind missing object of type {0}. Please fix it!", type));

                // Skip the removal
                return;
            }

            // Remove the binding
            bindingsByType.Remove(type);
        }

        public static void Bind(object o, string identifier)
        {
            // Safety to avoid multiple binding of the same type
            if (bindingsByName.ContainsKey(identifier))
            {
                // Show a message
                Debug.LogWarning(String.Format("Injector : Trying to rebind object with identifier {0}. Please fix it!", identifier));

                // Remove the previous binding
                bindingsByName.Remove(identifier);
            }

            // Set the new binding
            bindingsByName.Add(identifier, o);
        }

        public static void Unbind(string identifier)
        {
            // Safety when removing unexistant binding
            if (!bindingsByName.ContainsKey(identifier))
            {
                // Show a message
                Debug.LogWarning(String.Format("Injector : Trying to unbind missing object with identifier {0}. Please fix it!", identifier));

                // Skip the removal
                return;
            }

            // Remove the binding
            bindingsByName.Remove(identifier);
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class Inject : Attribute
    {
        // Name to identify the binding (optional)
        public string identifier = null;

        // Allow null bindings
        public bool allowNull = false;

        // Constructor empty
        public Inject(bool allowNull = false)
        {
            // Set allowNull
            this.allowNull = allowNull;
        }

        // Constructor with identifier
        public Inject(string identifier, bool allowNull = false)
        {
            // Set the identifier
            this.identifier = identifier;

            // Set allowNull
            this.allowNull = allowNull;
        }
    }
}