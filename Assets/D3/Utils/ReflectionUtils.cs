﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace D3
{
    public static class ReflectionUtils
    {
        public static List<FieldInfo> GetPublicFields(object o)
        {
            List<FieldInfo> result = new List<FieldInfo>();
            FieldInfo[] fields = o.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            for (int i = 0; i < fields.Length; i++)
            {
                if (!fields[i].IsNotSerialized)
                {
                    result.Add(fields[i]);
                }
            }
            return result;
        }

        public static List<T> GetFieldsOfType<T>(object o)
        {
            Type fieldType = typeof(T);
            List<T> result = new List<T>();
            FieldInfo[] fields = o.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            for (int i = 0; i < fields.Length; i++)
            {
                if (!fieldType.IsAssignableFrom(fields[i].FieldType)) continue;
                result.Add((T)fields[i].GetValue(o));
            }
            return result;
        }

        public static List<FieldInfo> GetFieldInfosOfType<T>(object o)
        {
            Type fieldType = typeof(T);
            FieldInfo[] fields = o.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            List<FieldInfo> result = new List<FieldInfo>();
            for (int i = 0; i < fields.Length; i++)
            {
                if (!fieldType.IsAssignableFrom(fields[i].FieldType)) continue;
                result.Add(fields[i]);
            }
            return result;
        }
    }
}