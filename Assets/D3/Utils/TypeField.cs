﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

namespace D3
{
    [Serializable]
    public class TypeField
    {
        [SerializeField]
        public string typeName = "";

        public Type type
        {
            get {return GetType(typeName);}
            set {typeName = value.FullName;}
        }

        public TypeField(Type t = null)
        {
            if(t != null)
            {
                type = t;
            }
        }

        public static Type GetType(string typeName)
        {
            return TypeUtils.GetTypeByName(typeName);
        }

        public override string ToString()
        {
            return typeName + " : " + type;
        }
    }
}

#if UNITY_EDITOR
namespace D3
{
    using UnityEditor;

    [CustomPropertyDrawer(typeof(TypeField))]
    public class TypeFieldDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Get the typeName property
            SerializedProperty typeNameProperty = property.FindPropertyRelative("typeName");

            // Get the name of the type
            string typeName = typeNameProperty.stringValue;

            EditorGUI.BeginProperty(position, label, property);

            Type type = TypeField.GetType(typeName);

            Rect bgPosition = new Rect(new Vector2(position.x + EditorGUIUtility.labelWidth, position.y), new Vector2(position.width - EditorGUIUtility.labelWidth, position.height));
            EditorGUI.DrawRect(bgPosition, type == null ? Color.red : Color.green);

            EditorGUI.PropertyField(position, typeNameProperty, label);

            EditorGUI.EndProperty();
        }
    }
}

#endif