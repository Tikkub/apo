﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace D3
{
    public class ReflectedField<T> where T : Attribute
    {
        // Type of the field
        public Type type;

        // Attribute instance
        public T attribute;

        // Field information
        public FieldInfo fieldInfo;
    }

    public class ReflectedType<T> where T : Attribute
    {
        // Dictionnary to cache reflection data of types
        private Dictionary<Type, ReflectedType<T>> reflections = new Dictionary<Type, ReflectedType<T>>();

        public ReflectedField<T>[] fields;

        public ReflectedType<T> Get(Type type)
        {
            // If the reflection is not in cache
            if (!reflections.ContainsKey(type))
            {
                // Cache it
                reflections.Add(type, Create(type));
            }

            // Return the reflection data
            return reflections[type];
        }

        private ReflectedType<T> Create(Type type)
        {
            // Create the holder for the reflection informations
            ReflectedType<T> reflectedType = new ReflectedType<T>();

            // Create a list of reflected field
            List<ReflectedField<T>> reflectedFields = new List<ReflectedField<T>>();

            // Get the list of class fields
            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

            // Browse the fields of the type
            for (int i = 0; i < fieldInfos.Length; i++)
            {
                // Get the field info 
                FieldInfo fieldInfo = fieldInfos[i];

                // Get the attribute of this field
                var attributes = fieldInfo.GetCustomAttributes(typeof(T), true);

                // If there is one take it because we can't have multiple inject attributes
                if (attributes.Length > 0)
                {
                    // Create the holder for field reflection data
                    ReflectedField<T> reflectedField = new ReflectedField<T>();

                    // Set the type of the field
                    reflectedField.type = fieldInfo.FieldType;

                    // Store the attribute
                    reflectedField.attribute = attributes[0] as T;

                    // Store the field infos
                    reflectedField.fieldInfo = fieldInfo;

                    // Store the field in the list
                    reflectedFields.Add(reflectedField);
                }
            }

            // Save the reflected fields in the type
            reflectedType.fields = reflectedFields.ToArray();

            // Return the reflection information
            return reflectedType;
        }
    }
}
