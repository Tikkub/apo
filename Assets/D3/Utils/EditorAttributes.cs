﻿using System;
using UnityEngine;

namespace D3
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class RangeStepAttribute : PropertyAttribute
    {
        public readonly int min;
        public readonly int max;
        public readonly int step;

        public RangeStepAttribute(int min, int max, int step)
        {
            this.min = min;
            this.max = max;
            this.step = step;
        }
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class RangePowerOfTwoAttribute : PropertyAttribute
    {
        public readonly int min;
        public readonly int max;

        public RangePowerOfTwoAttribute(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }

    public class ReadOnlyAttribute : PropertyAttribute
    {

    }

    public class OrderableArrayAttribute : PropertyAttribute
    {

    }
}