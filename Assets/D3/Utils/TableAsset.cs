﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace D3
{
    public class TableAsset<T> : ScriptableObject
    {
        public T[] entries;
    }
}