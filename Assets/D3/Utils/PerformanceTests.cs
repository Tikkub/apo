﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using Stopwatch = System.Diagnostics.Stopwatch;
using SystemRandom = System.Random;

namespace D3
{
    public static class PerformanceTests
    {
        // BITWISE DIVISION SLIGHTLY FASTER
        public static void TestDivision()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int loop = 1000000;
            int i;
            int powerOfTwo = 2;
            int size = Mathf.FloorToInt(Mathf.Pow(2, powerOfTwo));

            timer.Start();
            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += i / size;
            }

            Debug.Log(string.Format("Result 1 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += i >> powerOfTwo;
            }

            Debug.Log(string.Format("Result 2 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        // BITWISE MODULE SLIGTHLY FASTER
        public static void TestModulo()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int loop = 1000000;
            int i;
            int powerOfTwo = 2;
            int size = Mathf.FloorToInt(Mathf.Pow(2, powerOfTwo));
            int sizeMinus1 = size - 1;

            timer.Start();
            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += i % size;
            }

            Debug.Log(string.Format("Result 1 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += i & sizeMinus1;
            }

            Debug.Log(string.Format("Result 2 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        // NO REAL DIFFERENCE. SAME SAME
        public static void TestMultipleLoop()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int i, x, y, z, w;
            int powerOfTwo = 8;
            int powerOfTwo2 = powerOfTwo * 2;
            int size = Mathf.FloorToInt(Mathf.Pow(2, powerOfTwo));
            int sizeMinus1 = size - 1;
            int size2 = size * size;
            int size2Minus1 = size2 - 1;
            int size3 = size * size * size;

            timer.Start();
            result = 0;
            for (x = 0; x < size; x++)
            {
                for (y = 0; y < size; y++)
                {
                    for (z = 0; z < size; z++)
                    {
                        result += Mathf.Pow(Mathf.Sqrt(x + y + z) / 100, 2);
                    }
                }
            }

            Debug.Log(string.Format("Result 1 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();
            result = 0;
            for (i = 0; i < size3; i++)
            {
                x = i >> powerOfTwo2;
                w = i & size2Minus1;
                y = w >> powerOfTwo;
                z = w & sizeMinus1;
                result += Mathf.Pow(Mathf.Sqrt(x + y + z) / 100, 2);
            }

            Debug.Log(string.Format("Result 2 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        private struct IntContainer
        {
            public int x;
            public int y;
            public int z;

            public IntContainer(int x, int y, int z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }

            public int Sum()
            {
                return x + y + z;
            }
        }

        private static int IntMethod(int x, int y, int z)
        {
            return x + y + z;
        }

        private static int IntContainerMethod(IntContainer intContainer)
        {
            return intContainer.x + intContainer.y + intContainer.z;
        }

        // NO REAL DIFFERENCE. SAME SAME
        public static void TestIntVSIntContainer()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int i, loop = 1000000;
            int x = 1, y = 2, z = 3;
            IntContainer intContainer = new IntContainer(x, y, z);

            timer.Start();
            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += IntMethod(x, y, z);
            }

            Debug.Log(string.Format("Result 1 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += IntContainerMethod(intContainer);
            }

            Debug.Log(string.Format("Result 2 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += intContainer.Sum();
            }

            Debug.Log(string.Format("Result 3 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        // HUGE DIFFERENCE. USE SIMPLE FLAT ARRAY
        public static void TestReadMultiDimensionalArrayVSFlatArray()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int loop = 1000;
            int size = 16;
            int size2 = size * size;
            Int64[] flatArray = new Int64[size * size * size];
            Int64[,,] mulArray = new Int64[size, size, size];
            SystemRandom random = new SystemRandom();
            int i, x, y, z, value;

            for (x = 0; x < size; x++)
            {
                for (y = 0; y < size; y++)
                {
                    for (z = 0; z < size; z++)
                    {
                        value = random.Next(0, Int32.MaxValue);
                        flatArray[x + y * size + z * size2] = value;
                        mulArray[x, y, z] = value;
                    }
                }
            }


            timer.Start();
            result = 0;
            for (i = 0; i < loop; i++)
            {
                for (x = 0; x < size; x++)
                {
                    for (y = 0; y < size; y++)
                    {
                        for (z = 0; z < size; z++)
                        {
                            result += flatArray[x + y * size + z * size2];
                        }
                    }
                }
            }

            Debug.Log(string.Format("Read Flat = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                for (x = 0; x < size; x++)
                {
                    for (y = 0; y < size; y++)
                    {
                        for (z = 0; z < size; z++)
                        {
                            result += mulArray[x, y, z];
                        }
                    }
                }
            }

            Debug.Log(string.Format("Read Mul = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();
            result = 0;
            for (i = 0; i < loop; i++)
            {
                for (x = 0; x < size; x++)
                {
                    for (y = 0; y < size; y++)
                    {
                        for (z = 0; z < size; z++)
                        {
                            flatArray[x + y * size + z * size2] = 1;
                        }
                    }
                }
            }

            Debug.Log(string.Format("Write Flat = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();

            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                for (x = 0; x < size; x++)
                {
                    for (y = 0; y < size; y++)
                    {
                        for (z = 0; z < size; z++)
                        {
                            mulArray[x, y, z] = 1;
                        }
                    }
                }
            }

            Debug.Log(string.Format("Write Mul 4 = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        private class TestFuncClass
        {
            public int DoSomething(int i)
            {
                return i;
            }
        }

        // NO REAL DIFFERENCE. SAME SAME
        public static void TestFuncOverObjectCall()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int i, loop = 1000000;
            TestFuncClass o = new TestFuncClass();
            Func <int, int > DoSomething = o.DoSomething;

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += o.DoSomething(i);
            }

            Debug.Log(string.Format("Result object call = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += DoSomething(i);
            }

            Debug.Log(string.Format("Result func call = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

        }

        // SPEED RESULT : FLOOR CAST << FLOOR AND CEIL << ROUND
        // WINNER FLOOR CAST
        public static void TestFloorCeilRound()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int i, loop = 1000000;

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += Mathf.FloorToInt(UnityEngine.Random.value * 100);
            }

            Debug.Log(string.Format("Result floor = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += Mathf.CeilToInt(UnityEngine.Random.value * 100);
            }

            Debug.Log(string.Format("Result ceil = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += Mathf.RoundToInt(UnityEngine.Random.value * 100);
            }

            Debug.Log(string.Format("Result round = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += (int) (UnityEngine.Random.value * 100);
            }

            Debug.Log(string.Format("Result floor cast = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        private static int TestMethodCall()
        {
            return 2;
        }

        delegate int TestDelegate();

        public static void TestMethodCallPerformance()
        {
            Stopwatch timer = new Stopwatch();

            float result;
            int i, loop = 1000000;
            MethodInfo mi = ((Func<int>)TestMethodCall).Method;
            TestDelegate testDelegate = TestMethodCall;
            object[] p = new object[0];

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += TestMethodCall();
            }

            Debug.Log(string.Format("Result TestMethodCall = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += (int)mi.Invoke(null, p);
            }

            Debug.Log(string.Format("Result Invoke = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            result = 0;
            for (i = 0; i < loop; i++)
            {
                result += testDelegate();
            }

            Debug.Log(string.Format("Result testDelegate = {0} in {1}ms", result, timer.Elapsed.TotalMilliseconds));
        }

        public static void TestAddRemoveCollections()
        {
            Stopwatch timer = new Stopwatch();

            int i, loop = 1000000;

            timer.Reset();
            timer.Start();

            List<int> list = new List<int>(loop);
            for (i = 0; i < loop; i++)
            {
                list.Add(i);
            }

            Debug.Log(string.Format("List add = {0}ms", timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            for (i = 0; i < loop; i++)
            {
                list.RemoveAt(list.Count - 1);
            }

            Debug.Log(string.Format("List remove = {0}ms", timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            Stack<int> stack = new Stack<int>(loop);
            for (i = 0; i < loop; i++)
            {
                stack.Push(i);
            }

            Debug.Log(string.Format("Stack add = {0}ms", timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            for (i = 0; i < loop; i++)
            {
                stack.Pop();
            }

            Debug.Log(string.Format("Stack remove = {0}ms", timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            Queue<int> queue = new Queue<int>(loop);
            for (i = 0; i < loop; i++)
            {
                queue.Enqueue(i);
            }

            Debug.Log(string.Format("Queue add = {0}ms", timer.Elapsed.TotalMilliseconds));

            timer.Reset();
            timer.Start();

            for (i = 0; i < loop; i++)
            {
                queue.Dequeue();
            }

            Debug.Log(string.Format("Queue remove = {0}ms", timer.Elapsed.TotalMilliseconds));
        }
    }
}