﻿using System;
using UnityEngine.Events;

namespace D3
{
    public class UnityEventBool : UnityEvent<bool> { };
    public class UnityEventInt : UnityEvent<bool> { };
    public class UnityEventString : UnityEvent<string> { };
    public class UnityEventException : UnityEvent<Exception> { };
}
