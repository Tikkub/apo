﻿using UnityEngine;

namespace D3
{
    public class MathUtils
    {
        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360) angle += 360;
            else if (angle > 360) angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }
    }
}