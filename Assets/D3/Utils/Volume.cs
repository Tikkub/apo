﻿using UnityEngine;

namespace D3
{
    public class Volume
    {
        public float x;
        public float y;
        public float z;
        public float x2;
        public float y2;
        public float z2;

        public float xCenter;
        public float yCenter;
        public float zCenter;

        public float width;
        public float height;
        public float depth;

        public float halfWidth;
        public float halfHeight;
        public float halfDepth;

        public Vector3 center;
        public Vector3 position;
        public Vector3 dimensions;

        public Volume()
        {
        }

        public virtual void SetCorners(float xa, float ya, float za, float xb, float yb, float zb)
        {
            x = Mathf.Min(xa, xb);
            y = Mathf.Min(ya, yb);
            z = Mathf.Min(za, zb);
            x2 = Mathf.Max(xa, xb);
            y2 = Mathf.Max(ya, yb);
            z2 = Mathf.Max(za, zb);

            width = x2 - x;
            height = y2 - y;
            depth = z2 - z;

            halfWidth = width / 2;
            halfHeight = height / 2;
            halfDepth = depth / 2;

            xCenter = x + halfWidth;
            yCenter = y + halfHeight;
            zCenter = z + halfDepth;

            position.Set(x, y, z);
            dimensions.Set(width, height, depth);
            center.Set(xCenter, yCenter, zCenter);
        }

        public void SetCorners(Vector3 corner1, Vector3 corner2)
        {
            SetCorners(corner1.x, corner1.y, corner1.z, corner2.x, corner2.y, corner2.z);
        }

        public void SetPositionAndDimensions(float x, float y, float z, float width, float height, float depth)
        {
            SetCorners(x, y, z, x + width, y + height, z + depth);
        }

        public void SetPositionAndDimensions(Vector3 position, Vector3 dimensions)
        {
            SetCorners(position.x, position.y, position.z, position.x + dimensions.x, position.y + dimensions.y, position.z + dimensions.z);
        }

        public void SetCenterAndExtends(float x, float y, float z, float halfWidth, float halfHeight, float halfDepth)
        {
            SetCorners(x - halfWidth, y - halfHeight, z - halfDepth, x + halfWidth, y + halfHeight, z + halfDepth);
        }

        public void SetCenterAndExtends(Vector3 center, Vector3 halfDimensions)
        {
            SetCorners(center.x - halfDimensions.x, center.y - halfDimensions.y, center.z - halfDimensions.z, center.x + halfDimensions.x, center.y + halfDimensions.y, center.z + halfDimensions.z);
        }

        public void MoveTo(float x, float y, float z)
        {
            SetCorners(x, y, z, x + width, y + height, z + depth);
        }

        public void MoveTo(Vector3 position)
        {
            SetCorners(position.x, position.y, position.z, position.x + width, position.y + height, position.z + depth);
        }

        public void MoveBy(float vx, float vy, float vz)
        {
            SetCorners(x + vx, y + vy, z + vz, x + vx + width, y + vy + height, z + vz + depth);
        }

        public void MoveBy(Vector3 offset)
        {
            SetCorners(x + offset.x, y + offset.y, z + offset.z, x + offset.x + width, y + offset.y + height, z + offset.z + depth);
        }

        public void CenterTo(float x, float y, float z)
        {
            SetCorners(x - halfWidth, y - halfHeight, z - halfDepth, x + halfWidth, y + halfHeight, z + halfDepth);
        }

        public void CenterTo(Vector3 position)
        {
            SetCorners(position.x - halfWidth, position.y - halfHeight, position.z - halfDepth, position.x + halfWidth, position.y + halfHeight, position.z + halfDepth);
        }

        public bool ContainsPoint(float x, float y, float z, float boundary = 0)
        {
            return (x <= x2 - boundary)
                && (y <= y2 - boundary)
                && (z <= z2 - boundary)
                && (x >= this.x + boundary)
                && (y >= this.y + boundary)
                && (z >= this.z + boundary);
        }

        public bool ContainsPoint(Vector3 point, float boundary = 0)
        {
            return ContainsPoint(point.x, point.y, point.z, boundary);
        }

        public bool ContainsVolume(Volume volume, int boundary = 0)
        {
            return (volume.x2 <= x2 - boundary)
                && (volume.y2 <= y2 - boundary)
                && (volume.z2 <= z2 - boundary)
                && (volume.x >= x + boundary)
                && (volume.y >= y + boundary)
                && (volume.z >= z + boundary);
        }

        public bool Intersects(Volume volume)
        {
            if (x2 < volume.x || x > volume.x2) return false;
            if (y2 < volume.y || y > volume.y2) return false;
            if (z2 < volume.z || z > volume.z2) return false;
            return true;
        }

        public bool IntersectsOrTouch(Volume volume)
        {
            if (x2 <= volume.x || x >= volume.x2) return false;
            if (y2 <= volume.y || y >= volume.y2) return false;
            if (z2 <= volume.z || z >= volume.z2) return false;
            return true;
        }

        public bool Touch(Volume volume)
        {
            if (x2 == volume.x || x == volume.x2) return false;
            if (y2 == volume.y || y == volume.y2) return false;
            if (z2 == volume.z || z == volume.z2) return false;
            return true;
        }

        public void Grow(float vx, float vy, float vz)
        {
            SetCorners(x, y, z, x + Mathf.Max(1, width + vx), y + Mathf.Max(1, height + vy), z + Mathf.Max(1, depth + vz));
        }

        public void Grow(Vector3 v)
        {
            SetCorners(x, y, z, x + Mathf.Max(1, width + v.x), y + Mathf.Max(1, height + v.y), z + Mathf.Max(1, depth + v.z));
        }

        public void Shrink(float vx, float vy, float vz)
        {
            SetCorners(x, y, z, x + Mathf.Max(1, width - vx), y + Mathf.Max(1, height - vy), z + Mathf.Max(1, depth - vz));
        }

        public void Shrink(Vector3 v)
        {
            SetCorners(x, y, z, x + Mathf.Max(1, width - v.x), y + Mathf.Max(1, height - v.y), z + Mathf.Max(1, depth - v.z));
        }

        public override string ToString()
        {
            return $"{position} {dimensions}";
        }
    }

}
