﻿using UnityEngine;
using UnityRandom = UnityEngine.Random;
using SystemRandom = System.Random;

namespace D3
{
    public static class RandomUtils
    {
        public static SystemRandom systemRandom = new SystemRandom();

        public static float RandomFloat(float maxValue)
        {
            return UnityRandom.value * maxValue;
        }

        public static float RandomFloat(float minValue, float maxValue)
        {
            return minValue + UnityRandom.value * (maxValue - minValue);
        }

        public static int RandomInt(int maxValue)
        {
            return systemRandom.Next(maxValue);
        }

        public static int RandomInt(int minValue, int maxValue)
        {
            return systemRandom.Next(minValue, maxValue);
        }

        public static Vector3 RandomVector(float minValue, float maxValue)
        {
            return new Vector3(RandomFloat(minValue, maxValue), RandomFloat(minValue, maxValue), RandomFloat(minValue, maxValue));
        }

        public static T Random<T>(this T[] o)
        {
            return o[UnityRandom.Range(0, o.Length)];
        }

        public static float Value(this SystemRandom r)
        {
            return r.Next(1000000) / 1000000f;
        }
    }
}
