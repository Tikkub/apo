﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using D3;

namespace Apo
{
    [CustomEditor(typeof(ApoController))]
    public class ApoControllerEditor : Editor
    {
        private ApoKernel kernel;

        public void OnEnable()
        {
            ApoController apoController = (ApoController)target;
            kernel = apoController.kernel;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (!Application.isPlaying) return;

            string s = "";
            List<object> args = new List<object>();

            int argsIndex = 0;

            s += "Memory {" + (argsIndex++) + "} MB";
            args.Add(string.Format("{0:n0}", Mathf.Floor(System.GC.GetTotalMemory(false) / 1000000)));

            ApoBaseGroup entities = kernel.GetGroup<ApoBaseGroup>();
            s += "\r\n{" + (argsIndex++) + "} Entities";
            args.Add(entities.GetCount());

            List<Type> types = kernel.pools.GetTypes();
            foreach (Type type in types)
            {
                ApoPool pool = kernel.pools.Get(type);
                s += "\r\nPool {" + (argsIndex++) + "} : {" + (argsIndex++) + "}/{" + (argsIndex++) + "}";
                args.Add(type.Name);
                args.Add(pool.GetQuantity());
                args.Add(pool.GetCapacity());
            }

            foreach (Type type in kernel.componentTypes)
            {
                ApoGroup group = kernel.GetGroup(typeof(ApoGroup<>).MakeGenericType(type));
                s += "\r\nComponent {" + (argsIndex++) + "} : {" + (argsIndex++) + "}";
                args.Add(type.Name);
                args.Add(group.GetCount());
            }

            foreach (Type type in kernel.systemTypes)
            {
                s += "\r\nSystem {" + (argsIndex++) + "}";
                args.Add(type.Name);
            }

            EditorGUILayout.HelpBox(string.Format(s, args.ToArray()), MessageType.Info);
        }
    }
}