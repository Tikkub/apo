﻿using System;
using UnityEngine;
using D3;

namespace Apo.Example
{
    [ApoPoolAttribute(60000, 60000), Serializable]
    public class ScalerComponent : ApoSimpleComponent
    {
        public Vector3 speed;
        public Vector3 offset;
    }
}