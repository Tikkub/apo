﻿using System;
using UnityEngine;
using D3;

namespace Apo.Example
{
    [ApoPoolAttribute(100000, 100000), Serializable]
    public class MeshRendererComponent : ApoSimpleComponent
    {
        public Mesh mesh;
        public Material material;
        public Color color;
        public int textureId = 0;

        public override void Reset()
        {
            mesh = null;
            material = null;

            color.r = 1;
            color.g = 1;
            color.b = 1;
        }
    }
}