﻿using System;
using UnityEngine;
using D3;

namespace Apo.Example
{
    [ApoPoolAttribute(60000, 60000), Serializable]
    public class MovementComponent : ApoSimpleComponent
    {
        public Vector3 direction;
        public float speed;
        public float timeLeft;

        public override void Reset()
        {
            speed = 0.1f;
            timeLeft = -1;
        }
    }
}