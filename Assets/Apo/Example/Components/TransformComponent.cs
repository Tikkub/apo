﻿using System;
using UnityEngine;
using D3;

namespace Apo.Example
{
    [ApoPoolAttribute(100000, 100000), Serializable]
    public class TransformComponent : ApoSimpleComponent
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public override void Reset()
        {
            position.Set(0, 0, 0);
            rotation.Set(0, 0, 0, 1);
            scale.Set(1, 1, 1);
        }
    }
}