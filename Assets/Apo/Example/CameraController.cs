﻿using System;
using UnityEngine;

namespace Apo.Example
{
    public class CameraController : MonoBehaviour
    {
        public enum Mode
        {
            MENU,
            GOD,
        }

        public Transform cameraTransform;

        public float walkSpeed = 20f;
        public float runSpeed = 50f;
        public float rotationSpeed = 50f;

        private int yMinLimit = -90;
        private int yMaxLimit = 90;
        private float xAngle = 0.0f;
        private float yAngle = 0.0f;
        private float cooldown = 0;
        private float cooldownLength = 0.3f;

        public Mode mode = Mode.GOD;

        public float speed
        {
            get
            {
                return Input.GetKey(KeyCode.LeftShift) ? runSpeed : walkSpeed;
            }
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Init();
        }

        private void Init()
        {
            cameraTransform = GetComponentInChildren<Camera>().transform;

            xAngle = Vector3.Angle(Vector3.right, cameraTransform.right);
            yAngle = Vector3.Angle(Vector3.up, cameraTransform.up);

            SetMode(mode);
        }

        private void SetMode(Mode mode)
        {
            this.mode = mode;
            Cursor.lockState = mode == Mode.MENU ? CursorLockMode.None : CursorLockMode.Locked;
        }

        public void Update()
        {
            cooldown -= Time.deltaTime;

            if (Input.GetKey(KeyCode.BackQuote) && cooldown < 0)
            {
                SetMode(mode == Mode.GOD ? Mode.MENU : Mode.GOD);
                cooldown = cooldownLength;
            }

            if (mode == Mode.GOD)
            {
                Look();
                Move();
            }
        }

        private void Look()
        {
            xAngle += Input.GetAxis("Mouse X") * rotationSpeed;
            yAngle -= Input.GetAxis("Mouse Y") * rotationSpeed;
            yAngle = ClampAngle(yAngle, yMinLimit, yMaxLimit);
            //cameraTransform.rotation = Quaternion.Euler(yAngle, xAngle, 0);
            //transform.rotation = Quaternion.Euler(0, xAngle, 0);
            cameraTransform.rotation = Quaternion.Lerp(cameraTransform.rotation, Quaternion.Euler(yAngle, xAngle, 0), Time.deltaTime * 2);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, xAngle, 0), Time.deltaTime);
        }

        private void Move()
        {
            float hAxis = Input.GetAxis("Horizontal");
            float vAxis = Input.GetAxis("Vertical");
            float s = speed * Time.deltaTime;
            transform.position += transform.right * hAxis * s + cameraTransform.forward * vAxis * s;
        }

        private static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }
    }
}
