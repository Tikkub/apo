﻿using UnityEngine;

namespace Apo.Example
{
    public class MovementSystem : ApoSimpleSystem, ApoGroupProcessor<TransformComponent, MovementComponent>
    {
        private ApoGroup<TransformComponent, MovementComponent> entities;
        private int targetDistance = 100;
        private Vector3 target;
        private float deltaTime;

        public void OnUpdate(UpdateEvent e)
        {
            deltaTime = e.deltaTime;
            entities.Process(this);
        }

        public void Process(TransformComponent location, MovementComponent movement, int entity, int index, int count)
        {
            if (movement.timeLeft < 0)
            {
                target = new Vector3(Random.value * targetDistance, Random.value * targetDistance, Random.value * targetDistance);
                movement.direction = target - location.position;
                movement.timeLeft = movement.direction.magnitude / movement.speed;
                movement.direction.Normalize();
            }

            location.position += movement.direction * movement.speed * deltaTime;
            movement.timeLeft -= deltaTime;
        }
    }
}