﻿using System;
using UnityEngine;

namespace Apo.Example
{
    public class ScalerSystem : ApoSimpleSystem, ApoGroupProcessor<TransformComponent, ScalerComponent>
    {
        private ApoGroup<TransformComponent, ScalerComponent> entities;
        private float time;
        private Func<float, float> Cos;

        public override void Init()
        {
            Cos = Mathf.Cos;
        }

        public void OnUpdate(UpdateEvent e)
        {
            time += e.deltaTime;

            entities.Process(this);
        }

        public void Process(TransformComponent transform, ScalerComponent scaler, int entity, int index, int count)
        {
            transform.scale.x = 1 + Cos(scaler.offset.x + time * scaler.speed.x) * 0.5f;
            transform.scale.y = 1 + Cos(scaler.offset.y + time * scaler.speed.y) * 0.5f;
            transform.scale.z = 1 + Cos(scaler.offset.z + time * scaler.speed.z) * 0.5f;
        }
    }
}