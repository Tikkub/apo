﻿using UnityEngine;

namespace Apo.Example
{
    public class MeshRendererSystem : ApoSimpleSystem, ApoGroupProcessor<TransformComponent, MeshRendererComponent>
    {
        private static int COLOR_ARRAY_ID = Shader.PropertyToID("_Color");
        private static int TEXTURE_ARRAY_ID = Shader.PropertyToID("_TextureId");

        public bool shadow = false;

        private ApoGroup<TransformComponent, MeshRendererComponent> entities;

        private Matrix4x4[] matrices = new Matrix4x4[1023];
        private Vector4[] colors = new Vector4[1023];
        private float[] textureIds = new float[1023];
        private int i = 0;
        private MaterialPropertyBlock properties = new MaterialPropertyBlock();

        public void OnUpdate(UpdateEvent e)
        {
            i = 0;
            entities.Process(this); 
        }

        public void Process(TransformComponent transform, MeshRendererComponent meshRenderer, int entity, int index, int count)
        {
            matrices[i] = Matrix4x4.TRS(transform.position, transform.rotation, transform.scale);
            colors[i] = meshRenderer.color;
            textureIds[i] = meshRenderer.textureId;
            i++;
            
            if (index == count - 1 || i == 1023)
            {
                properties.SetVectorArray(COLOR_ARRAY_ID, colors);
                properties.SetFloatArray(TEXTURE_ARRAY_ID, textureIds);
                Graphics.DrawMeshInstanced(meshRenderer.mesh, 0, meshRenderer.material, matrices, i, properties, shadow ? UnityEngine.Rendering.ShadowCastingMode.On : UnityEngine.Rendering.ShadowCastingMode.Off, shadow, 0);
                i = 0;
            }
        }
    }
}