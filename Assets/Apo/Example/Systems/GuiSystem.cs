﻿using System;
using System.Collections.Generic;
using UnityEngine;
using D3;

namespace Apo.Example
{
    public class GuiSystem : ApoSimpleSystem
    {
        private float time = 0.4f;

        private MeshRendererSystem meshRendererSystem = null;
        public ApoBaseGroup entities = null;
        private int margin = 5;
        private int width = 150;
        private int height = 20;
        private Rect rect;
        private ApoExample example;

        private void OnDrawGui(DrawGuiEvent e)
        {
            rect.Set(margin, margin, width, height);

            GUI.Label(rect, $"FPS: {example.fps}"); JumpLine();

            GUI.Label(rect, $"RAM: {Mathf.Floor(GC.GetTotalMemory(false) / 1000000)} MB"); JumpLine();

            GUI.Label(rect, $"Time: {time}"); JumpLine();
            SetTime(GUI.HorizontalSlider(rect, time, 0, 1)); JumpLine();

            GUI.Label(rect, $"Entities: {entities.GetCount()}"); JumpLine();
            example.numberOfEntities = (int)GUI.HorizontalSlider(rect, entities.GetCount(), 0, kernel.entityLimit); JumpLine();

            meshRendererSystem.shadow = GUI.Toggle(rect, meshRendererSystem.shadow, "Shadows"); JumpLine();
            example.entityMaterial.SetFloat("_ColorEnabled", GUI.Toggle(rect, example.entityMaterial.GetFloat("_ColorEnabled") == 1, "Colors") ? 1 : 0); JumpLine();
            example.entityMaterial.SetFloat("_TextureEnabled", GUI.Toggle(rect, example.entityMaterial.GetFloat("_TextureEnabled") == 1, "Textures") ? 1 : 0); JumpLine();

            if (GUI.Button(rect, "Destroy all entities")) { example.DestroyEntities(example.numberOfEntities); } JumpLine();
            if (GUI.Button(rect, "Create 1000 entities")){ example.CreateEntities(1000); } JumpLine();
            if (GUI.Button(rect, "Create 10000 entities")) { example.CreateEntities(10000); } JumpLine();
            if (GUI.Button(rect, "Create 100000 entities")) { example.CreateEntities(100000); } JumpLine();
        }

        private void JumpLine()
        {
            rect.y += margin + height;
        }

        private void SetTime(float t)
        {
            time = t;
            example.sun.transform.rotation = Quaternion.Euler(time * 360 - 90, -30, 0);
            example.sun.intensity = 1 - Mathf.Abs(time * 2 - 1f);
        }
    }
}