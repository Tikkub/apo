﻿using UnityEngine;
using D3;

namespace Apo.Example
{
    public class ApoExampleEntityBuilder : ApoEntityBuilder
    {
        public Mesh mesh;
        public Material material;

        public ApoExampleEntityBuilder(Mesh mesh, Material material)
        {
            this.mesh = mesh;
            this.material = material;
        }

        public void Build(int entity, ApoKernel kernel)
        {
            TransformComponent transformComponent = kernel.AddComponent<TransformComponent>(entity);
            transformComponent.position.Set(Random.value * 100, Random.value * 100, Random.value * 100);
            transformComponent.scale.Set(0.5f + Random.value, 0.5f + Random.value, 0.5f + Random.value);

            MeshRendererComponent meshRendererComponent = kernel.AddComponent<MeshRendererComponent>(entity);
            meshRendererComponent.mesh = mesh;
            meshRendererComponent.material = material;
            meshRendererComponent.textureId = 1 + (int)(Random.value * 31);
            meshRendererComponent.color.r = Random.value;
            meshRendererComponent.color.g = Random.value;
            meshRendererComponent.color.b = Random.value;

            if (RandomUtils.RandomFloat(1) < 0.5)
            {
                MovementComponent movementComponent = kernel.AddComponent<MovementComponent>(entity);
                movementComponent.speed = 1 + Random.value * 9f;
            }
            else
            {
                ScalerComponent scalerComponent = kernel.AddComponent<ScalerComponent>(entity);
                scalerComponent.speed.Set(3 + Random.value * 7, 3 + Random.value * 7, 3 + Random.value * 7);
                scalerComponent.offset.Set(Random.value * 100, Random.value * 100, Random.value * 100);
            }
        }
    }
}