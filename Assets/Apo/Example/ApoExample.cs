﻿using System.Collections;
using UnityEngine;

namespace Apo.Example
{
    public class ApoExample : ApoMonoBehaviourSystem
    {
        public ApoController apoController;
        public Mesh entityMesh;
        public Material entityMaterial;
        public Texture2D[] textures;
        public int numberOfEntities = 100;
        public Light sun;

        [HideInInspector]
        public float fps;

        private ApoBaseGroup entities = null;
        private ApoExampleEntityBuilder entityBuilder;

        private void Start()
        {
            ApoKernel kernel = apoController.kernel;
            kernel.AddSystem(new MovementSystem());
            kernel.AddSystem(new MeshRendererSystem());
            kernel.AddSystem(new ScalerSystem());
            kernel.AddSystem(this);
            kernel.AddSystem(new GuiSystem());
        }

        public override void Init()
        {
            InitMaterial();
            entityBuilder = new ApoExampleEntityBuilder(entityMesh, entityMaterial);
            StartCoroutine(CheckFps()); 
        }

        private void InitMaterial()
        {
            int materialCount = textures.Length;
            Texture2DArray textureArray = new Texture2DArray(16, 16, materialCount + 1, TextureFormat.RGBA32, false);

            for (int i = 0; i < materialCount; i++)
            {
                Texture texture = textures[i];
                Graphics.CopyTexture(texture, 0, 0, textureArray, i+1, 0);
            }

            textureArray.filterMode = FilterMode.Point;
            textureArray.wrapMode = TextureWrapMode.Repeat;

            entityMaterial.SetTexture("_Textures", textureArray);
        }

        private void UpdateEventHandler(UpdateEvent e)
        {
            int diff = numberOfEntities - entities.GetCount();
            if (diff > 0)
            {
                for (int i = 0; i < diff; i++)
                {
                    kernel.CreateEntity(entityBuilder);
                }
            }
            else if (diff < 0)
            {
                for (int i = 0; i < -diff; i++)
                {
                    kernel.DestroyEntity(entities.GetLast());
                }
            }
        }

        public void DestroyEntities(int amount)
        {
            numberOfEntities = Mathf.Max(0, numberOfEntities - amount);
        }

        public void CreateEntities(int amount)
        {
            numberOfEntities = Mathf.Min(kernel.entityLimit, numberOfEntities + amount);
        }

        private IEnumerator CheckFps()
        {
            while (true)
            {
                if (Time.timeScale > 0)
                {
                    yield return new WaitForSeconds(0.1f);
                    fps = Mathf.Round(Time.timeScale / Time.deltaTime);
                }
                else
                {
                    fps = 0;
                }
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
}