﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Apo/ApoExampleShader" {
	Properties {
		_Textures("Textures", 2DArray) = "" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		[MaterialToggle]
		_ColorEnabled("Color enabled", Float) = 1
		[MaterialToggle]
		_TextureEnabled("Texture enabled", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.5
		#pragma instancing_options assumeuniformscaling maxcount:1023
		#include "UnityCG.cginc"

		UNITY_DECLARE_TEX2DARRAY(_Textures);

		struct Input {
			fixed2 uv_Textures; 
		};

		fixed _ColorEnabled;
		fixed _TextureEnabled;
		half _Glossiness;
		half _Metallic;

		UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
#define _Color_arr Props
			UNITY_DEFINE_INSTANCED_PROP(float, _TextureId)
#define _TextureId_arr Props
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, _TextureEnabled ? UNITY_ACCESS_INSTANCED_PROP(_TextureId_arr, _TextureId) : 0)) * (_ColorEnabled ? UNITY_ACCESS_INSTANCED_PROP(_Color_arr, _Color) : fixed4(1,1,1,1));
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
