﻿using D3;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Apo
{
    public class ApoKernel
    {
        #region Members

        // Dimensions
        public readonly int entityLimit = 100000;

        // Systems
        public Type systemInterfaceType = typeof(ApoSystem);
        public Type[] systemTypes;
        private Dictionary<Type, ApoSystem> systems = new Dictionary<Type, ApoSystem>();

        // Entities
        private bool[] entities;
        private Stack<int> availableEntities;

        // Components
        public Type componentInterfaceType = typeof(ApoComponent);
        public Type[] componentTypes;
        private Dictionary<Type, ApoComponent[]> components = new Dictionary<Type, ApoComponent[]>();

        // Groups
        public Type groupInterfaceType = typeof(ApoGroup);
        public Type[] groupTypes;
        private Dictionary<Type, ApoGroup> groups = new Dictionary<Type, ApoGroup>();

        // Events
        public Type eventInterfaceType = typeof(ApoEvent);
        public Type[] eventTypes;
        private Dictionary<Type, ApoEventDispatcher> listeners = new Dictionary<Type, ApoEventDispatcher>();
        private Queue<ApoEvent> pendingEvents = new Queue<ApoEvent>();
        private Queue<ApoEvent> processingEvents = new Queue<ApoEvent>();

        // Pools
        public ApoPools pools = new ApoPools();

        #endregion

        #region Cycle

        public void Init()
        {
            InitComponents();
            InitEntities();
            InitSystems();
            InitEvents();
            InitGroups();
        }

        public void Update()
        {
            ProcessEvents();
        }

        #endregion

        #region Systems

        private void InitSystems()
        {
            systemTypes = TypeUtils.FindTypeAssignableFor(systemInterfaceType).ToArray();
        }

        public ApoSystem AddSystem(ApoSystem system)
        {
            systems.Add(system.GetType(), system);
            InjectGroups(system);
            InjectSystems(system);
            AddListeners(system);
            system.OnAdded(this);
            return system;
        }

        public ApoSystem AddSystem(Type type)
        {
            return AddSystem(Activator.CreateInstance(type) as ApoSystem);
        }

        public T AddSystem<T>() where T : class, ApoSystem
        {
            return AddSystem(typeof(T)) as T;
        }

        public ApoSystem RemoveSystem(ApoSystem system)
        {
            system.OnRemoved(this);
            RemoveListeners(system);
            systems.Remove(system.GetType());
            return system;
        }

        public void RemoveSystem(Type type)
        {
            RemoveSystem(GetSystem(type));
        }

        public void RemoveSystem<T>() where T : class, ApoSystem
        {
            RemoveSystem(typeof(T));
        }

        public ApoSystem GetSystem(Type type)
        {
            ApoSystem system;
            systems.TryGetValue(type, out system);
            return system;
        }

        public T GetSystem<T>() where T : class, ApoSystem
        {
            return GetSystem(typeof(T)) as T;
        }

        public List<ApoSystem> GetSystems()
        {
            return new List<ApoSystem>(systems.Values);
        }

        private void InjectSystems(ApoSystem system)
        {
            foreach (FieldInfo fieldInfo in system.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (!systemInterfaceType.IsAssignableFrom(fieldInfo.FieldType)) continue;
                fieldInfo.SetValue(system, GetSystem(fieldInfo.FieldType));
            }
        }

        #endregion

        #region Entities

        private void InitEntities()
        {
            entities = new bool[entityLimit];
            availableEntities = new Stack<int>(entityLimit);
            for (int i = entityLimit - 1; i > -1; i--)
            {
                availableEntities.Push(i);
            }
        }

        public int CreateEntity(ApoEntityBuilder builder = null)
        {
#if UNITY_EDITOR
            if (availableEntities.Count == 0)
            {
                UnityEngine.Debug.LogWarning("Entity pool empty! Increase the entity limit");
                return -1;
            }
#endif
            int entity = availableEntities.Pop();
            builder?.Build(entity, this);
            entities[entity] = true;
            UpdateGroups(entity);
            return entity;
        }

        public T CreateEntity<T>(ApoEntityBuilder builder = null) where T : class, ApoComponent
        {
            return GetComponent<T>(CreateEntity(builder)) as T;
        }

        public void DestroyEntity(int entity)
        {
            foreach (Type componentType in componentTypes)
            {
                RemoveComponent(entity, componentType, false);
            }
            availableEntities.Push(entity);
            entities[entity] = false;
            RemoveFromGroups(entity);
        }

        public bool IsEntityAlive(int entity)
        {
            return entities[entity];
        }

        #endregion

        #region Components

        private void InitComponents()
        {
            componentTypes = TypeUtils.FindTypeAssignableFor(componentInterfaceType).ToArray();

            foreach (Type type in componentTypes)
            {
                components.Add(type, Array.CreateInstance(type, entityLimit) as ApoComponent[]);
                ApoPoolAttribute attribute = type.GetCustomAttribute<ApoPoolAttribute>();
                if (attribute == null) attribute = new ApoPoolAttribute();
                pools.Init(type, attribute.initialCapacity, attribute.initialQuantity);
            }
        }

        // Create
        public T CreateEntityWithComponent<T>() where T : class, ApoComponent
        {
            return CreateEntityWithComponent(typeof(T)) as T;
        }

        public ApoComponent CreateEntityWithComponent(Type type)
        {
            int entity = CreateEntity();
            return AddComponent(entity, type);
        }

        // GetAll
        public ApoComponent[] GetAllComponents(Type type)
        {
            return components[type];
        }

        public T[] GetAllComponents<T>() where T : class, ApoComponent
        {
            return GetAllComponents(typeof(T)) as T[];
        }

        // Get
        public ApoComponent GetComponent(int entity, Type type)
        {
            return components[type][entity];
        }

        public T GetComponent<T>(int entity) where T : class, ApoComponent
        {
            return GetComponent(entity, typeof(T)) as T;
        }

        public List<ApoComponent> GetComponents(int entity)
        {
            List<ApoComponent> list = new List<ApoComponent>();
            foreach (Type componentType in componentTypes)
            {
                ApoComponent component = components[componentType][entity];
                if (component != null)
                {
                    list.Add(component);
                }
            }
            return list;
        }

        // Has
        public bool HasComponent(int entity, Type type)
        {
            return components[type][entity] != null;
        }

        public bool HasComponent<T>(int entity) where T : class, ApoComponent
        {
            return HasComponent(entity, typeof(T));
        }

        // Add
        public ApoComponent AddComponent(int entity, Type type, bool notifyGroups = true)
        {
            ApoComponent component = pools.Get(type).Extract() as ApoComponent;
            components[type][entity] = component;
            if (notifyGroups)
            {
                UpdateGroups(entity);
            }
            return component;
        }

        public T AddComponent<T>(int entity, bool notifyGroups = true) where T : class, ApoComponent
        {
            return AddComponent(entity, typeof(T), notifyGroups) as T;
        }

        // Remove
        public void RemoveComponent(int entity, Type type, bool notifyGroups = true)
        {
            ApoComponent component = components[type][entity];
            if (component == null) return;
            components[type][entity] = null;
            if (notifyGroups)
            {
                UpdateGroups(entity);
            }
            pools.Get(type).Insert(component);
        }

        public void RemoveComponent<T>(int entity, bool notifyGroups = true) where T : class, ApoComponent
        {
            RemoveComponent(entity, typeof(T), notifyGroups);
        }

        #endregion

        #region Groups

        private void InitGroups()
        {
            groupTypes = TypeUtils.FindTypeAssignableFor(groupInterfaceType).ToArray();
        }

        public T GetGroup<T>() where T : class, ApoGroup
        {
            return GetGroup(typeof(T)) as T;
        }

        public ApoGroup GetGroup(Type type)
        {
            ApoGroup group;
            if (!groups.TryGetValue(type, out group))
            {
                group = Activator.CreateInstance(type, new object[] { this }) as ApoGroup;
                groups.Add(type, group);
                for (int entity = 0; entity < entityLimit; entity++)
                {
                    if (IsEntityAlive(entity))
                    {
                        UpdateGroups(entity);
                    }
                }
            }
            return group;
        }

        public IEnumerable<Type> GetAllGroupTypes()
        {
            return groups.Keys;
        }

        private void InjectGroups(ApoSystem system)
        {
            foreach (FieldInfo fieldInfo in system.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (!groupInterfaceType.IsAssignableFrom(fieldInfo.FieldType)) continue;
                fieldInfo.SetValue(system, GetGroup(fieldInfo.FieldType));
            }
        }

        private void RemoveFromGroups(int entity)
        {
            foreach (ApoGroup group in groups.Values)
            {
                group.Remove(entity);
            }
        }

        public void UpdateGroups(int entity)
        {
            foreach (ApoGroup group in groups.Values)
            {
                group.Check(entity);
            }
        }

        #endregion

        #region Events

        private void InitEvents()
        {
            eventTypes = TypeUtils.FindTypeAssignableFor(eventInterfaceType).ToArray();
            foreach (Type type in eventTypes)
            {
                listeners.Add(type, Activator.CreateInstance(typeof(ApoEventDispatcher<>).MakeGenericType(type)) as ApoEventDispatcher);
                ApoPoolAttribute attribute = type.GetCustomAttribute<ApoPoolAttribute>();
                if (attribute == null) attribute = new ApoPoolAttribute();
                pools.Init(type, attribute.initialCapacity, attribute.initialQuantity);
            }
        }

        public ApoEvent CreateEvent(Type type)
        {
            return pools.Get(type).Extract() as ApoEvent;
        }

        public T CreateEvent<T>() where T : class, ApoEvent
        {
            return CreateEvent(typeof(T)) as T;
        }

        public void DispatchEvent<T>(T e) where T : class, ApoEvent
        {
            pendingEvents.Enqueue(e);
        }

        public void DispatchEvent<T>() where T : class, ApoEvent
        {
            pendingEvents.Enqueue(CreateEvent<T>());
        }

        public void DispatchEventRightNow<T>(T e) where T : class, ApoEvent
        {
            ProcessEvent(e);
        }

        public void DispatchEventRightNow<T>() where T : class, ApoEvent
        {
            ProcessEvent(CreateEvent<T>());
        }

        private void ProcessEvents()
        {
            Queue<ApoEvent> temp = processingEvents;
            processingEvents = pendingEvents;
            pendingEvents = temp;

            while (processingEvents.Count > 0)
            {
                ProcessEvent(processingEvents.Dequeue());
            }
        }

        private void ProcessEvent(ApoEvent e)
        {
            Type type = e.GetType();
            listeners[type].Dispatch(e);
            pools.Get(type).Insert(e);
        }

        public void AddListeners(object o)
        {
            foreach (MethodInfo methodInfo in o.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (methodInfo.GetParameters().Length != 1) continue;
                Type eventType = methodInfo.GetParameters()[0].ParameterType;
                if (!eventInterfaceType.IsAssignableFrom(eventType)) continue;
                listeners[eventType].AddListener(o, methodInfo);
            }
        }

        public void RemoveListeners(object o)
        {
            foreach (MethodInfo methodInfo in o.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (methodInfo.GetParameters().Length != 1) continue;
                Type eventType = methodInfo.GetParameters()[0].ParameterType;
                if (!eventInterfaceType.IsAssignableFrom(eventType)) continue;
                listeners[eventType].RemoveListener(o, methodInfo);
            }
        }

        private interface ApoEventDispatcher
        {
            void AddListener(object o, MethodInfo d);
            void RemoveListener(object o, MethodInfo d);
            void Dispatch(ApoEvent e);
        }

        private class ApoEventDispatcher<T> : ApoEventDispatcher where T : class, ApoEvent
        {
            private Action<T> handler = delegate { };

            public void AddListener(object o, MethodInfo methodName)
            {
                handler += methodName.CreateDelegate(typeof(Action<T>), o) as Action<T>;
            }

            public void RemoveListener(object o, MethodInfo methodName)
            {
                handler -= methodName.CreateDelegate(typeof(Action<T>), o) as Action<T>;
            }

            public void Dispatch(ApoEvent e)
            {
                handler.Invoke(e as T);
            }
        }

        #endregion
    }
}