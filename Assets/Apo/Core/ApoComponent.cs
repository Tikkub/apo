﻿using D3;
using System;

namespace Apo
{
    public interface ApoComponent : ApoPoolObject
    {
    }

    public abstract class ApoSimpleComponent : ApoComponent
    {
        public ApoSimpleComponent()
        {
            Reset();
        }

        public virtual void Reset()
        {
        }
    }
}