﻿using UnityEngine;

namespace Apo
{
    public interface ApoSystem
    {
        void Init();
        void OnAdded(ApoKernel kernel);
        void OnRemoved(ApoKernel kernel);
    }

    public abstract class ApoSimpleSystem : ApoSystem
    {
        protected ApoKernel kernel;

        public virtual void Init()
        {
        }

        public virtual void OnAdded(ApoKernel kernel)
        {
            this.kernel = kernel;
            Init();
        }

        public virtual void OnRemoved(ApoKernel kernel)
        {
        }
    }

    public abstract class ApoMonoBehaviourSystem : MonoBehaviour, ApoSystem
    {
        protected ApoKernel kernel;

        public virtual void Init()
        {
        }

        public virtual void OnAdded(ApoKernel kernel)
        {
            this.kernel = kernel;
            Init();
        }

        public virtual void OnRemoved(ApoKernel kernel)
        {
        }
    }
}