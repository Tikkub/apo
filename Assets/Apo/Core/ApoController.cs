﻿using UnityEngine;

namespace Apo
{
    public class ApoController : MonoBehaviour
    {
        public ApoKernel kernel = new ApoKernel();

        private void Awake()
        {
            kernel.Init();
        }

        private void Update()
        {
            kernel.DispatchEvent(kernel.CreateEvent<UpdateEvent>().Set(Time.deltaTime, Time.time, Time.frameCount));
            kernel.Update();
        }

        private void FixedUpdate()
        {
            kernel.DispatchEventRightNow<FixedUpdateEvent>();
        }

        private void LateUpdate()
        {
            kernel.DispatchEventRightNow<LateUpdateEvent>();
        }

        private void OnApplicationFocus(bool focus)
        {
            kernel.DispatchEventRightNow(kernel.CreateEvent<ApplicationFocusEvent>().Set(focus));
        }

        private void OnApplicationPause(bool pause)
        {
            kernel.DispatchEventRightNow(kernel.CreateEvent<ApplicationPauseEvent>().Set(pause));
        }

        private void OnApplicationQuit()
        {
            kernel.DispatchEventRightNow<ApplicationQuitEvent>();
        }

        private void OnGUI()
        {
            kernel.DispatchEventRightNow<DrawGuiEvent>();
        }

        private void OnDestroy()
        {
            kernel.DispatchEventRightNow<DestroyEvent>();
        }

        #if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                kernel.DispatchEventRightNow<DrawGizmosEvent>();
            }
        }
        #endif
    }
}