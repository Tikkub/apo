﻿using D3;
using System;

namespace Apo
{
    public interface ApoEvent : ApoPoolObject
    {
    }

    public abstract class ApoSimpleEvent : ApoEvent
    {
        public ApoSimpleEvent()
        {
            Reset();
        }

        public virtual void Reset()
        {
        }
    }
}