﻿namespace Apo
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Collections.Generic;
    using UnityEngine.Events;

    public interface ApoGroup
    {
        void Remove(int entity);
        void Check(int entity);
        int GetFirst();
        int GetLast();
        int GetCount();
        IEnumerable<int> GetAll();
    }

    public interface ApoGroupProcessor
    {
        void Process(int entity, int index, int count);
    }

    public class EntityEvent : UnityEvent<int> { }

    public class ApoBaseGroup : ApoGroup
    {
        public Action<int> onEntityAdded;
        public Action<int> onEntityRemoved;

        protected int[] entitiesList;
        protected int[] entitiesIndex;
        protected int count = 0;
        protected int entity;

        public ApoBaseGroup(ApoKernel kernel)
        {
            entitiesList = new int[kernel.entityLimit];
            entitiesIndex = new int[kernel.entityLimit];
            for (int i = 0; i < entitiesIndex.Length; i++)
            {
                entitiesIndex[i] = -1;
            }
        }

        public virtual bool IsMember(int entity)
        {
            return true;
        }

        public bool Contains(int entity)
        {
            return entitiesIndex[entity] != -1;
        }

        private void Add(int entity)
        {
            if (Contains(entity)) return;
            entitiesList[count] = entity;
            entitiesIndex[entity] = count++;
            onEntityAdded?.Invoke(entity);
        }

        public void Remove(int entity)
        {
            if (!Contains(entity)) return;

            int entity1 = entity;
            int entity2 = entitiesList[--count];

            int listIndex1 = entitiesIndex[entity1];
            int listIndex2 = entitiesIndex[entity2];

            entitiesList[listIndex1] = entitiesList[listIndex2];

            entitiesIndex[entity2] = listIndex1;
            entitiesIndex[entity1] = -1;

            onEntityRemoved?.Invoke(entity);
        }

        public void Check(int entity)
        {
            if (IsMember(entity))
            {
                Add(entity);
            }
            else
            {
                Remove(entity);
            }
        }

        public void Process(ApoGroupProcessor processor)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                processor.Process(entity, i, count);
            }
        }

        public void Process(Action<int, int, int> action)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                action(entity, i, count);
            }
        }

        public int GetCount()
        {
            return count;
        }

        public int GetFirst()
        {
            return entitiesList[0];
        }

        public int GetLast()
        {
            return entitiesList[count - 1];
        }

        public IEnumerable<int> GetAll()
        {
            return new ArraySegment<int>(entitiesList, 0, count);
        }
    }

    public interface ApoGroupProcessor<C1>
    {
        void Process(C1 component1, int entity, int index, int count);
    }

    public class ApoGroup<C1> : ApoBaseGroup
        where C1 : class, ApoComponent
    {
        protected C1[] components1;

        public ApoGroup(ApoKernel kernel) : base(kernel)
        {
            components1 = kernel.GetAllComponents<C1>();
        }

        public override bool IsMember(int entity)
        {
            if (components1[entity] == null) return false;
            return true;
        }

        public void Process(ApoGroupProcessor<C1> processor)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                processor.Process(components1[entity], entity, i, count);
            }
        }

        public void Process(Action<C1, int, int, int> action)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                action(components1[entity], entity, i, count);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public C1 GetComponent1(int entity)
        {
            return components1[entity];
        }
    }

    public interface ApoGroupProcessor<C1, C2>
    {
        void Process(C1 component1, C2 component2, int entity, int index, int count);
    }

    public class ApoGroup<C1, C2> : ApoGroup<C1>
        where C1 : class, ApoComponent
        where C2 : class, ApoComponent
    {
        protected C2[] components2;

        public ApoGroup(ApoKernel kernel) : base(kernel)
        {
            components1 = kernel.GetAllComponents<C1>();
            components2 = kernel.GetAllComponents<C2>();
        }

        public override bool IsMember(int entity)
        {
            if (components1[entity] == null) return false;
            if (components2[entity] == null) return false;
            return true;
        }

        public void Process(ApoGroupProcessor<C1, C2> processor)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                processor.Process(components1[entity], components2[entity], entity, i, count);
            }
        }

        public void Process(Action<C1, C2, int, int, int> action)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                action(components1[entity], components2[entity], entity, i, count);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public C2 GetComponent2(int entity)
        {
            return components2[entity];
        }
    }

    public interface ApoGroupProcessor<C1, C2, C3>
    {
        void Process(C1 component1, C2 component2, C3 component3, int entity, int index, int count);
    }

    public class ApoGroup<C1, C2, C3> : ApoGroup<C1, C2>
        where C1 : class, ApoComponent
        where C2 : class, ApoComponent
        where C3 : class, ApoComponent
    {
        protected C3[] components3;

        public ApoGroup(ApoKernel kernel) : base(kernel)
        {
            components1 = kernel.GetAllComponents<C1>();
            components2 = kernel.GetAllComponents<C2>();
            components3 = kernel.GetAllComponents<C3>();
        }

        public override bool IsMember(int entity)
        {
            if (components1[entity] == null) return false;
            if (components2[entity] == null) return false;
            if (components3[entity] == null) return false;
            return true;
        }

        public void Process(ApoGroupProcessor<C1, C2, C3> processor)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                processor.Process(components1[entity], components2[entity], components3[entity], entity, i, count);
            }
        }

        public void Process(Action<C1, C2, C3, int, int, int> action)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                action(components1[entity], components2[entity], components3[entity], entity, i, count);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public C3 GetComponent3(int entity)
        {
            return components3[entity];
        }
    }

    public interface ApoGroupProcessor<C1, C2, C3, C4>
    {
        void Process(C1 component1, C2 component2, C3 component3, C4 component4, int entity, int index, int count);
    }

    public class ApoGroup<C1, C2, C3, C4> : ApoGroup<C1, C2, C3>
        where C1 : class, ApoComponent
        where C2 : class, ApoComponent
        where C3 : class, ApoComponent
        where C4 : class, ApoComponent
    {
        public delegate void ProcessEntityC4(C1 component0, C2 component1, C3 component2, C4 component3, int entity, int index, int count);
        protected C4[] components4;

        public ApoGroup(ApoKernel kernel) : base(kernel)
        {
            components1 = kernel.GetAllComponents<C1>();
            components2 = kernel.GetAllComponents<C2>();
            components3 = kernel.GetAllComponents<C3>();
            components4 = kernel.GetAllComponents<C4>();
        }

        public override bool IsMember(int entity)
        {
            if (components1[entity] == null) return false;
            if (components2[entity] == null) return false;
            if (components3[entity] == null) return false;
            if (components4[entity] == null) return false;
            return true;
        }

        public void Process(ApoGroupProcessor<C1, C2, C3, C4> processor)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                processor.Process(components1[entity], components2[entity], components3[entity], components4[entity], entity, i, count);
            }
        }

        public void Process(Action<C1, C2, C3, C4, int, int, int> action)
        {
            for (int i = 0; i < count; i++)
            {
                entity = entitiesList[i];
                action(components1[entity], components2[entity], components3[entity], components4[entity], entity, i, count);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public C4 GetComponent4(int entity)
        {
            return components4[entity];
        }
    }


    public class ApoGroupOptional<C1, C2Optional> : ApoGroup<C1, C2Optional>
        where C1 : class, ApoComponent
        where C2Optional : class, ApoComponent
    {
        public ApoGroupOptional(ApoKernel kernel) : base(kernel) {}

        public override bool IsMember(int entity)
        {
            if (components1[entity] == null) return false;
            return true;
        }
    }
}