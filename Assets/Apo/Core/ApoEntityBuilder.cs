﻿namespace Apo
{
    public interface ApoEntityBuilder
    {
        void Build(int entity, ApoKernel kernel);
    }
}