﻿using UnityEngine;

namespace Apo
{
    public class ApoCameraController : MonoBehaviour
    {
        public ApoController apoController;
        public ApoKernel kernel;

        private void Start()
        {
            kernel = apoController.kernel;            
        }

        private void OnPreCull()
        {
            kernel.DispatchEventRightNow<PreCullEvent>();
        }

        private void OnPreRender()
        {
            kernel.DispatchEventRightNow<PreRenderEvent>();
        }

        private void OnPostRender()
        {
            kernel.DispatchEventRightNow<PostRenderEvent>();
        }

        private void OnRenderObject()
        {
            kernel.DispatchEventRightNow<RenderObjectEvent>();
        }
    }
}