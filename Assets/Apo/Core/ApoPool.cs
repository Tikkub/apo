﻿using System;
using System.Collections.Generic;

namespace Apo
{
    public interface ApoPoolObject
    {
        void Reset();
    }

    public class ApoPool
    {
        private ApoPoolObject[] objects;
        private int quantity = 0;
        private Type type;

        public ApoPool(Type type, int capacity, int quantity)
        {
            this.type = type;
            objects = new ApoPoolObject[capacity];
            Fill(quantity);
        }

        public ApoPoolObject Create(bool reset = true)
        {
            ApoPoolObject o = Activator.CreateInstance(type) as ApoPoolObject;
            if (reset)
            {
                o.Reset();
            }
            return o;
        }

        public ApoPoolObject Extract()
        {
            if (quantity == 0)
            {
                return Create();
            }
            return objects[--quantity];
        }

        public void Insert(ApoPoolObject o)
        {
            o.Reset();
            if (quantity >= objects.Length)
            {
                Array.Resize(ref objects, (int)(objects.Length * 1.5f));
            }
            objects[quantity++] = o;
        }

        public void Fill(int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                Insert(Create(false));
            }
        }

        public int GetQuantity()
        {
            return quantity;
        }

        public int GetCapacity()
        {
            return objects.Length;
        }
    }

    public class ApoPools
    {
        private Dictionary<Type, ApoPool> pools = new Dictionary<Type, ApoPool>();

        public void Init(Type type, int capacity = 1000, int quantity = 1000)
        {
            pools.Add(type, Activator.CreateInstance(typeof(ApoPool), new object[] { type, capacity, quantity }) as ApoPool);
        }

        public ApoPool Get(Type type)
        {
            return pools[type] as ApoPool;
        }

        public List<Type> GetTypes()
        {
            return new List<Type>(pools.Keys);
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ApoPoolAttribute : Attribute
    {
        public int initialCapacity;
        public int initialQuantity;

        public ApoPoolAttribute(int initialCapacity = 10, int initialQuantity = 10)
        {
            this.initialCapacity = initialCapacity;
            this.initialQuantity = initialQuantity;
        }
    }
}