﻿namespace Apo
{
    public class UpdateEvent : ApoSimpleEvent
    {
        public float deltaTime;
        public float time;
        public int frame;

        public override void Reset()
        {
            Set(0, 0, 0);
        }

        public UpdateEvent Set(float deltaTime, float time, int frame)
        {
            this.deltaTime = deltaTime;
            this.time = time;
            this.frame = frame;
            return this;
        }
    }
}