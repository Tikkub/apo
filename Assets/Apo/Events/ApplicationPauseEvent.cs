﻿namespace Apo
{
    public class ApplicationPauseEvent : ApoSimpleEvent
    {
        public bool pause;

        public override void Reset()
        {
            Set(false);
        }

        public ApplicationPauseEvent Set(bool pause)
        {
            this.pause = pause;
            return this;
        }
    }
}