﻿namespace Apo
{
    public class ApplicationFocusEvent : ApoSimpleEvent
    {
        public bool focus;

        public override void Reset()
        {
            Set(false);
        }

        public ApplicationFocusEvent Set(bool focus)
        {
            this.focus = focus;
            return this;
        }
    }
}